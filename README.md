# Logic & intro to programming

- https://www.edx.org/course/introduction-computer-science-mitx-6-00-1x-9

- https://py.checkio.org

- visualize - http://pythontutor.com/visualize.html#mode=edit 

# Quick intro to python

- http://www.pythonforbeginners.com/basics/python-quick-guide

- https://learnxinyminutes.com/docs/python/

- 5h google intro https://www.youtube.com/watch?v=tKTZoB2Vjuk&feature=channel


# Advanced topics

## Decorators
https://realpython.com/blog/python/primer-on-python-decorators/

## Generators
https://realpython.com/blog/python/introduction-to-python-generators/

## Inner functions
https://realpython.com/blog/python/inner-functions-what-are-they-good-for/

## String formating in python
https://pyformat.info/

## Type annotations
https://www.caktusgroup.com/blog/2017/02/22/python-type-annotations/

## Packaging

- http://www.pythonforbeginners.com/basics/how-to-use-pip-and-pypi
- Virtual env: https://virtualenv.pypa.io/en/stable/

## py.test
http://pythontesting.net/framework/pytest/pytest-introduction/

## PEP8
- https://www.python.org/dev/peps/pep-0008/

- https://pypi.python.org/pypi/pep8

- http://pep8online.com/


# Must watch

- https://github.com/s16h/py-must-watch

# Docs

- http://www.tutorialspoint.com/python/
- https://docs.python.org/3/


# Daily news

- Nice python podcast: https://talkpython.fm/

- http://www.reddit.com/r/python